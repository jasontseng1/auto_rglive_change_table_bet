# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from Lib.library_functions import LibraryFunctions as lf


class LiveBaccaratChangeTableBet:
    """ RG真人_百家樂換桌下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def lobby_change_table_bet(self):
        """ 大廳換桌下注 """

        # init
        all_table = {}
        close_sound = False
        l_f = lf(self.driver, self.get_img)

        # 點擊進入百家樂
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='百家樂']"))).click()
        time.sleep(2)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-select']//span[contains(text(), '百家樂')]")))
        for _ in ele_list:
            all_table[_.text.split(' ')[-1]] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        print(f"==================== 測試項目：「大廳換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「百家樂 {table}」下注")

            # 點擊進入百家樂
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='club']//div[text()='百家樂']"))).click()
            time.sleep(2)

            try:
                # 進桌
                _ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, f"//div[@class='desk-info']/div[./span[text()=' 百家樂 {table}']]/..")))
                self.driver.execute_script("arguments[0].scrollIntoView();", _ele)
                _ele.click()
                time.sleep(1)

                # 判斷是否進桌
                WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                    (By.XPATH, f"//div[@id='bacc']//div[@class='game-name' and contains(text(), '{table}')]")))

                # P系列桌別才需要判斷
                if not close_sound and 'P' in table:
                    # 關閉現場音效
                    try:
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@class='button no']"))).click()
                        close_sound = True
                    except:
                        pass

                print(f"進入「百家樂-{table}」成功")
            except:
                self.get_img(f"Fail：進入「百家樂-{table}」失敗")
                continue

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], "//*[name()='path' and @class='player']")

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result

    def game_inside_change_table_bet(self):
        """ 遊戲內換桌下注 """

        # init
        all_table = {}
        close_sound = False
        l_f = lf(self.driver, self.get_img)

        # 點擊進入百家樂
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='百家樂']"))).click()
        time.sleep(2)

        # 先進第一桌
        WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//span[contains(text(), '百家樂')]/../..")))[0].click()

        # 點開選桌
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@id='float-btn']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='icon classicBacc']"))).click()
        time.sleep(1)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-list']//span[contains(text(), '百家樂')]")))
        for _ in ele_list:
            all_table[_.text.split(' ')[-1]] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        # 取得當前
        here_table = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
            (By.XPATH, "//span[text()='您在此桌']/../.."))).text
        first_table = list(all_table.keys())[0]
        now_table = ''
        # 判斷當前是否是第一桌
        if first_table in here_table:
            now_table = first_table

        print(f"==================== 測試項目：「遊戲內換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「百家樂 {table}」下注")
            try:
                # 點開選單
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@id='float-btn']"))).click()
                time.sleep(1)

                # 判斷選桌是否顯示
                _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@id='change-table']")))

                # 當前桌不用在進桌
                if table != now_table:
                    if "open" not in _ele.get_attribute("class"):
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@id='float-btn']"))).click()
                        time.sleep(1)

                    # 點選百家樂
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, "//div[@class='icon classicBacc']"))).click()
                    time.sleep(1)

                    # 進桌
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//div[@id='desk-list']//span[text()=' 百家樂 {table}']/../.."))).click()

                # 判斷是否進桌
                WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                    (By.XPATH, f"//div[@id='bacc']//div[@class='game-name' and contains(text(), '{table}')]")))

                # P系列桌別才需要判斷
                if not close_sound and 'P' in table:
                    # 關閉現場音效
                    try:
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@class='button no']"))).click()
                        close_sound = True
                    except:
                        pass

                print(f"進入「百家樂-{table}」成功")
            except:
                self.get_img(f"Fail：進入「百家樂-{table}」失敗")
                continue

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], "//*[name()='path' and @class='player']")

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result
