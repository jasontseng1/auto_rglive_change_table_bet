# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from Lib.library_functions import LibraryFunctions as lf


class LiveBlockChainChangeTableBet:
    """ RG真人_區塊鏈換桌下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def lobby_change_table_bet(self):
        """ 大廳換桌下注 """

        # init
        all_table = {}
        l_f = lf(self.driver, self.get_img)

        # 點擊進入區塊鏈
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='區塊鏈']"))).click()
        time.sleep(2)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-select']//div[@class='desk-info']//span[@class='text']")))
        for _ in ele_list:
            game_name = _.text
            # 額外處理
            if '百家樂' in game_name:
                ele = _.find_element(By.XPATH, "./..")
                if 'hbcbacc' in ele.get_attribute('class'):
                    game_name = '性感' + _.text

            # 桌名都不同，故不需切割
            all_table[game_name] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        print(f"==================== 測試項目：「大廳換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「{table}」下注")

            # 點擊進入區塊鏈
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='club']//div[text()='區塊鏈']"))).click()
            time.sleep(2)

            try:
                title_xpath = "//div[@class='desk-info']"
                if '性感' in table and '百家樂' in table:
                    game_xpath = f"{title_xpath}/div[contains(@class, 'hbcbacc') and ./span[text()=' {table[2:]}']]/.."
                elif '百家樂' in table:
                    game_xpath = f"{title_xpath}/div[not(contains(@class, 'hbcbacc')) and ./span[text()=' {table}']]/.."
                else:
                    game_xpath = f"{title_xpath}/div[./span[text()=' {table}']]/.."

                # 進桌
                _ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, game_xpath)))
                self.driver.execute_script("arguments[0].scrollIntoView();", _ele)
                _ele.click()
                time.sleep(1)

                _xpath = ""
                if '射龍門' in table:
                    _xpath = "//div[@id='bcsdd']"
                elif '龍虎' in table:
                    _xpath = "//div[@id='dragon-tiger']"
                elif '性感' in table and '百家樂':
                    _xpath = "//div[@id='hbcbacc']"
                elif '百家樂' in table:
                    _xpath = "//div[@id='bacc']"

                # 判斷是否進桌
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                    (By.XPATH, f"{_xpath}//div[@class='game-name' and contains(text(), '{table[-3:]}')]")))

                print(f"進入「{table}」成功")
            except:
                self.get_img(f"Fail：進入「{table}」失敗")
                continue

            _xpath = ""
            if '射龍門' in table:
                _xpath = "//*[name()='path' and @class='goal']"
            elif '龍虎' in table:
                _xpath = "//*[name()='path' and @class='dragon']"
            elif '百家樂' in table:
                _xpath = "//*[name()='path' and @class='player']"

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], _xpath)

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result

    def game_inside_change_table_bet(self):
        """ 遊戲內換桌下注 """

        # init
        all_table = {}
        l_f = lf(self.driver, self.get_img)

        # 點擊進入區塊鏈
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='區塊鏈']"))).click()
        time.sleep(2)

        # 先進第一桌
        WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-select']//div[@class='desk-info']//span[@class='text']/../..")))[0].click()

        # 點開選桌
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@id='float-btn']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='icon blockbaccarat']"))).click()
        time.sleep(1)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-list']//span[@class='text']")))
        for _ in ele_list:
            game_name = _.text
            # 額外處理
            if '百家樂' in game_name:
                ele = _.find_element(By.XPATH, "./..")
                if 'hbcbacc' in ele.get_attribute('class'):
                    game_name = '性感區塊鏈' + _.text
                else:
                    game_name = '區塊鏈' + _.text

            # 桌名都不同，故不需切割
            all_table[game_name] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        # 取得當前
        here_table = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
            (By.XPATH, "//span[text()='您在此桌']/../..")))
        here_table_name = here_table.text.split('\n')[0]
        first_table = list(all_table.keys())[0]
        now_table = ''
        # 判斷當前是否是第一桌
        if 'sexy' in here_table.get_attribute('class'):
            if '性感' in first_table and here_table_name in first_table:
                now_table = first_table
        else:
            if first_table in here_table_name:
                now_table = first_table

        print(f"==================== 測試項目：「遊戲內換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「{table}」下注")
            try:
                # 點開選單
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@id='float-btn']"))).click()
                time.sleep(1)

                # 判斷選桌是否顯示
                _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@id='change-table']")))

                # 當前桌不用在進桌
                if table != now_table:
                    if "open" not in _ele.get_attribute("class"):
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@id='float-btn']"))).click()
                        time.sleep(1)

                    # 點選區塊鏈
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, "//div[@class='icon blockbaccarat']"))).click()
                    time.sleep(1)

                    title_xpath = "//div[@id='desk-list']"
                    if '性感' in table and '百家樂' in table:
                        game_xpath = f"{title_xpath}//div[contains(@class, 'hbcbacc')]/span[text()=' {table[-7:]}']/../.."
                    elif '百家樂' in table:
                        game_xpath = f"{title_xpath}//div[not(contains(@class, 'hbcbacc'))]/span[text()=' {table[-7:]}']/../.."
                    else:
                        game_xpath = f"{title_xpath}//span[text()=' {table}']/../.."

                    # 進桌
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, game_xpath))).click()

                _xpath = ""
                if '射龍門' in table:
                    _xpath = "//div[@id='bcsdd']"
                elif '龍虎' in table:
                    _xpath = "//div[@id='dragon-tiger']"
                elif '性感' in table and '百家樂':
                    _xpath = "//div[@id='hbcbacc']"
                elif '百家樂' in table:
                    _xpath = "//div[@id='bacc']"

                # 判斷是否進桌
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                    (By.XPATH, f"{_xpath}//div[@class='game-name' and contains(text(), '{table[-3:]}')]")))

                print(f"進入「{table}」成功")
            except:
                self.get_img(f"Fail：進入「{table}」失敗")
                continue

            _xpath = ""
            if '射龍門' in table:
                _xpath = "//*[name()='path' and @class='goal']"
            elif '龍虎' in table:
                _xpath = "//*[name()='path' and @class='dragon']"
            elif '百家樂' in table:
                _xpath = "//*[name()='path' and @class='player']"

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], _xpath)

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result
