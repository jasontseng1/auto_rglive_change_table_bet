# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from Lib.library_functions import LibraryFunctions as lf


class LiveSicBoChangeTableBet:
    """ RG真人_骰寶換桌下注 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    def lobby_change_table_bet(self):
        """ 大廳換桌下注 """

        # init
        all_table = {}
        l_f = lf(self.driver, self.get_img)

        # 點擊進入骰寶
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='骰寶']"))).click()
        time.sleep(2)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-select']//span[contains(text(), '骰')]")))
        for _ in ele_list:
            # 桌名都不同，故不需切割
            all_table[_.text] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        print(f"==================== 測試項目：「大廳換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「{table}」下注")

            # 點擊進入骰寶
            WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='club']//div[text()='骰寶']"))).click()
            time.sleep(2)

            try:
                # 進桌
                _ele = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, f"//div[@class='desk-info']/div[./span[text()=' {table}']]/..")))
                self.driver.execute_script("arguments[0].scrollIntoView();", _ele)
                _ele.click()
                time.sleep(1)

                _xpath = ""
                if '骰寶' in table:
                    _xpath = "//div[@id='sicbo']"
                elif '泰國骰' in table:
                    _xpath = "//div[@id='hilo']"

                # 判斷是否進桌
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                    (By.XPATH, f"{_xpath}//div[@class='game-name' and contains(text(), '{table[4:]}')]")))

                print(f"進入「{table}」成功")
            except:
                self.get_img(f"Fail：進入「{table}」失敗")
                continue

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], "//*[name()='path' and @class='single_1']")

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result

    def game_inside_change_table_bet(self):
        """ 遊戲內換桌下注 """

        # init
        all_table = {}
        l_f = lf(self.driver, self.get_img)

        # 點擊進入骰寶
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='club']//div[text()='骰寶']"))).click()
        time.sleep(2)

        # 先進第一桌
        WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//span[contains(text(), '骰')]/../..")))[0].click()

        # 點開選桌
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@id='float-btn']"))).click()
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='icon sicbo']"))).click()
        time.sleep(1)

        # 取得所有桌
        ele_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_any_elements_located(
            (By.XPATH, "//div[@id='desk-list']//span[contains(text(), '骰')]")))
        for _ in ele_list:
            # 桌名都不同，故不需切割
            all_table[_.text] = {
                'bet_finish': False,
                'report_check': False,
                'round': '',
                'balance': '',
            }

        # 取得當前
        here_table = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
            (By.XPATH, "//span[text()='您在此桌']/../.."))).text
        first_table = list(all_table.keys())[0]
        now_table = ''
        # 判斷當前是否是第一桌
        if first_table in here_table:
            now_table = first_table

        print(f"==================== 測試項目：「遊戲內換桌下注」 ==================== \n")
        for table in all_table:
            print(f"測試桌別：「{table}」下注")
            try:
                # 點開選單
                WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@id='float-btn']"))).click()
                time.sleep(1)

                # 判斷選桌是否顯示
                _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@id='change-table']")))

                # 當前桌不用在進桌
                if table != now_table:
                    if "open" not in _ele.get_attribute("class"):
                        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//div[@id='float-btn']"))).click()
                        time.sleep(1)

                    # 點選骰寶
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, "//div[@class='icon sicbo']"))).click()
                    time.sleep(1)

                    # 進桌
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, f"//div[@id='desk-list']//span[text()=' {table}']/../.."))).click()

                _xpath = ""
                if '骰寶' in table:
                    _xpath = "//div[@id='sicbo']"
                elif '泰國骰' in table:
                    _xpath = "//div[@id='hilo']"

                # 判斷是否進桌
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located(
                    (By.XPATH, f"{_xpath}//div[@class='game-name' and contains(text(), '{table[4:]}')]")))

                print(f"進入「{table}」成功")
            except:
                self.get_img(f"Fail：進入「{table}」失敗")
                continue

            # 下注&確認餘額
            all_table[table] = l_f.bet_and_check_balance(all_table[table], "//*[name()='path' and @class='single_1']")

        # 檢查下注資料
        result = l_f.check_report(all_table)
        return result
