# -*- coding: utf-8 -*-
"""
A TestRunner for use with the Python unit testing framework. It
generates a HTML report to show the result at a glance.

The simplest way to use this is to invoke its main method. E.g.

    import unittest
    import HTMLTestRunner

    ... define your tests ...

    if __name__ == '__main__':
        HTMLTestRunner.main()


For more customization options, instantiates a HTMLTestRunner object.
HTMLTestRunner is a counterpart to unittest's TextTestRunner. E.g.

    # output to a file
    fp = file('my_report.html', 'wb')
    runner = HTMLTestRunner.HTMLTestRunner(
                stream=fp,
                title='My unit test',
                description='This demonstrates the report output by HTMLTestRunner.'
                )

    # Use an external stylesheet.
    # See the Template_mixin class for more customizable options
    runner.STYLESHEET_TMPL = '<link rel="stylesheet" href="my_stylesheet.css" type="text/css">'

    # run the test
    runner.run(my_test_suite)


------------------------------------------------------------------------
Copyright (c) 2004-2007, Wai Yip Tung
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

* Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name Wai Yip Tung nor the names of its contributors may be
  used to endorse or promote products derived from this software without
  specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER
OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# URL: http://tungwaiyip.info/software/HTMLTestRunner.html

__author__ = "Wai Yip Tung,  Findyou"
__version__ = "0.8.2.2"


"""
Change History
Version 0.8.2.1 -Findyou
* 改为支持python3

Version 0.8.2.1 -Findyou
* 支持中文，汉化
* 调整样式，美化（需要连入网络，使用的百度的Bootstrap.js）
* 增加 通过分类显示、测试人员、通过率的展示
* 优化“详细”与“收起”状态的变换
* 增加返回顶部的锚点

Version 0.8.2
* Show output inline instead of popup window (Viorel Lupu).

Version in 0.8.1
* Validated XHTML (Wolfgang Borgert).
* Added description of test classes and test cases.

Version in 0.8.0
* Define Template_mixin class for customization.
* Workaround a IE 6 bug that it does not treat <script> block as CDATA.

Version in 0.7.1
* Back port to Python 2.3 (Frank Horowitz).
* Fix missing scroll bars in detail log (Podi).
"""

# TODO: color stderr
# TODO: simplify javascript using ,ore than 1 class in the class attribute?

import datetime
import io
import sys
import base64
import time
import unittest
from PIL import Image
from unittest import TestResult
from xml.sax import saxutils
import sys
import re

# run_time_count - Self added to run multi currency
# initil = 0
run_time_count = 0

# ------------------------------------------------------------------------
# The redirectors below are used to capture output during testing. Output
# sent to sys.stdout and sys.stderr are automatically captured. However
# in some cases sys.stdout is already cached before HTMLTestRunner is
# invoked (e.g. calling logging.basicConfig). In order to capture those
# output, use the redirectors for the cached stream.
#
# e.g.
#   >>> logging.basicConfig(stream=HTMLTestRunner.stdout_redirector)
#   >>>


class OutputRedirector(object):
    """ Wrapper to redirect stdout or stderr """

    def __init__(self, fp):
        self.fp = fp

    def write(self, s):
        self.fp.write(s)

    def writelines(self, lines):
        self.fp.writelines(lines)

    def flush(self):
        self.fp.flush()


stdout_redirector = OutputRedirector(sys.stdout)
stderr_redirector = OutputRedirector(sys.stderr)

# ----------------------------------------------------------------------
# Template


class Template_mixin(object):
    """
    Define a HTML template for report customerization and generation.

    Overall structure of an HTML report

    HTML
    +------------------------+
    |<html>                  |
    |  <head>                |
    |                        |
    |   STYLESHEET           |
    |   +----------------+   |
    |   |                |   |
    |   +----------------+   |
    |                        |
    |  </head>               |
    |                        |
    |  <body>                |
    |                        |
    |   HEADING              |
    |   +----------------+   |
    |   |                |   |
    |   +----------------+   |
    |                        |
    |   REPORT               |
    |   +----------------+   |
    |   |                |   |
    |   +----------------+   |
    |                        |
    |   ENDING               |
    |   +----------------+   |
    |   |                |   |
    |   +----------------+   |
    |                        |
    |  </body>               |
    |</html>                 |
    +------------------------+
    """

    STATUS = {
        0: 'pass',
        1: 'fail',
        2: 'error',
    }

    DEFAULT_TITLE = 'Test Report'
    DEFAULT_DESCRIPTION = ''
    DEFAULT_TESTER = 'QA'

    # ------------------------------------------------------------------------
    # HTML Template

    HTML_TMPL = r"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>%(title)s</title>
    <meta name="generator" content="%(generator)s"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-2.0.0.min.js" integrity="sha256-1IKHGl6UjLSIT6CXLqmKgavKBXtr0/jJlaGMEkh+dhw=" crossorigin="anonymous"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    %(stylesheet)s
</head>
<body >
<script language="javascript" type="text/javascript">
output_list = Array();
var gobalIndex;
var GlassSwitch = false;

/*level 调整增加只显示通过用例的分类 --Findyou
0:Summary //all hiddenRow
1:Failed  //pt hiddenRow, ft none, et hiddenRow
2:Pass    //pt none, ft hiddenRow, et hiddenRow
3:Error    //pt hiddenRow, ft hiddenRow, et none
4:All     //pt none, ft none, et none
*/
function showCase(level) {
    trs = document.getElementsByTagName("tr");
    for (var i = 0; i < trs.length; i++) {
        tr = trs[i];
        id = tr.id;
        if (id.substr(0,2) == 'ft') {
            if (level == 0 || level == 2 || level == 3) {
                tr.className = 'hiddenRow';
            }
            else {
                tr.className = '';
            }
        }
        if (id.substr(0,2) == 'pt') {
            if (level < 2 || level == 3) {
                tr.className = 'hiddenRow';
            }
            else {
                tr.className = '';
            }
        }
        if (id.substr(0,2) == 'et') {
            if ( level == 0 || level == 1 || level == 2) {
                tr.className = 'hiddenRow';
            }
            else {
                tr.className = '';
            }
        }
    }

    //加入【详细】切换文字变化 --Findyou
    detail_class=document.getElementsByClassName('detail');
	//console.log(detail_class.length)
	if (level == 4) {
		for (var i = 0; i < detail_class.length; i++){
			detail_class[i].innerHTML="隱藏"
		}
	}
	else{
			for (var i = 0; i < detail_class.length; i++){
			detail_class[i].innerHTML="展開"
		}
	}
}

function showClassDetail(cid, count) {
    var id_list = []
    var toHide = 1;
    var case_row = cid.substr(1);

    // 動態抓出 指定的 Case 群組內所有測試項目的行名稱
    $('#result_table tr').each((i,v) => {
      if(v.id.includes(case_row+'_'))
        id_list.push(v.id)
    })

    for (var i = 0; i < count; i++) {
        tr = document.getElementById(id_list[i]);
        if (tr.className) {
            toHide = 0;
        }
    }
    for (var i = 0; i < count; i++) {
        tid = id_list[i];
        //修改点击无法收起的BUG，加入【详细】切换文字变化 --Findyou
        if (toHide) {
            document.getElementById(tid).className = 'hiddenRow';
            document.getElementById(cid).innerText = "展開"
        }
        else {
            document.getElementById(tid).className = '';
            document.getElementById(cid).innerText = "隱藏"
        }
    }
}

function html_escape(s) {
    s = s.replace(/&/g,'&amp;');
    s = s.replace(/</g,'&lt;');
    s = s.replace(/>/g,'&gt;');
    return s;
}

function goLightBox(e) {
    float_html = '';
    imgsUrl = []
    var url = e.children[0].src;
    var bro_count = e.parentElement.children.length;

    $(e.parentElement).children().each((i,v) => { imgsUrl.push($(v).children('img').attr('src')) })

    imgsUrl.forEach((v,i) => {
        float_html += '<div class="column" onclick="imageSwitch(0,'+ i +')"><img class="indicat" src="'+v+'" /></div>'
    })

    $('#indicators').html(float_html)
    showModal();
    imageSwitch(0,imgsUrl.indexOf(url))
    gobalIndex = imgsUrl.indexOf(url)
}

function imageSwitch(sw, assignIndex) {
    let max = imgsUrl.length - 1;

    switch(sw){
        case 0:
            $('#lightbox img').attr('src',imgsUrl[assignIndex]);
            $('#bigImg').attr('src',imgsUrl[assignIndex]);
            gobalIndex = assignIndex;
            break;
        case 1:
            if(gobalIndex < max){
                $('#lightbox img').attr('src',imgsUrl[gobalIndex+1]);
                $('#bigImg').attr('src',imgsUrl[gobalIndex+1]);
                gobalIndex += 1;
            }
            break;
        case -1:
            if(gobalIndex > 0){
                $('#lightbox img').attr('src',imgsUrl[gobalIndex-1]);
                $('#bigImg').attr('src',imgsUrl[gobalIndex-1]);
                gobalIndex -= 1;
            }
            break;
    }

    changeLight(gobalIndex);
}

function showModal() {
    document.getElementById('modalbox').style.display = 'block';
    $('#indicators').attr('style','margin-left: -'+ $('#indicators').width()/2 +'px;')
}

function closeModal() {
    document.getElementById('modalbox').style.display = 'none';
}

function changeLight(index) {
    var indicators = document.getElementsByClassName('indicat');
    var i = 0;

    for (var item of indicators) {
        item.classList.remove('current');
        if (i === index)
            item.classList.add('current');
        i += 1;
    }
}

function switchChange(e){
    var oSmall = document.getElementById('lightbox');
    GlassSwitch = $('#'+e.id+' input').val() == 1 ? true : false;

    if(GlassSwitch)
        oSmall.classList.add('zoom-in')
    else
        oSmall.classList.remove('zoom-in')
}

window.onload = function(){
    //  獲取到所有的元素,因為有對應元素的移動,所以在設定樣式的,一定要給對應的元素絕對定位,類似於拖拽
    var oSmall = document.getElementById('lightbox'),
    oMask = document.getElementById('mask'),
    oBig = document.getElementById('bigLightbox'),
    oBigImg = document.getElementById('bigImg');

    //  給當前小的div一個滑鼠移入事件
    oSmall.onmouseover = function(){
    //  當滑鼠移入時,對應的區域顯示 小的觀察框，和 對應的右面的放大圖片
        oMask.style.display = GlassSwitch ? 'block': '';
        oBig.style.display = GlassSwitch ? 'block': '';
    };

    oSmall.onmouseout = function(){
    //  當滑鼠移除時對應的區域隱藏
        oMask.style.display = 'none';
        oBig.style.display = 'none';
    }

    oSmall.onmousemove = function(ev){
        //  首先獲取到event事件
        var oEvent = ev || event;
        // 當視窗置中相較於最左邊的寬度距離
        var ml =  $('.light').offset().left;//(window.innerWidth - oSmall.offsetWidth) / 2;
        // 當視窗置中相較於最頂邊的寬度距離
        var mt = Number($('.modal-body').css("marginTop").replace('px','')) + Number($('.modal-body').css("paddingTop").replace('px', ''));

        //  滑鼠距離當前視窗左邊的距離 了l ,就為當前滑鼠距離視窗左邊的距離 減去 小的觀察框的寬度
        var l = (oEvent.clientX - ml) - oMask.offsetWidth / 2;
        var t = (oEvent.clientY - mt) - oMask.offsetHeight / 2;


        //  對觀察框(左右)距離的限制, 1.當它距離左邊的距離比0 小的時候,設定它為0 就是它移動到最左邊的時候
        if (l < 0)
            l = 0;
        else
            if (l > oSmall.offsetWidth - oMask.offsetWidth)
                l = oSmall.offsetWidth - oMask.offsetWidth;
        // 同理對上下邊界進行設定
        if (t < 0)
            t = 0;
        else
            if (t > oSmall.offsetHeight - oMask.offsetHeight)
                t = oSmall.offsetHeight - oMask.offsetHeight;
        // console.log('滑鼠X座標',oEvent.clientX,'滑鼠Y座標',oEvent.clientY, '遮罩top位置：',t)
        //  設定小的觀察框的移動時的當前位置
        oMask.style.left = l + 'px';
        oMask.style.top = t + 'px';
        //  設定大的 img 邊框會跟著滑鼠跑
        oBig.style.left = oEvent.clientX + 10 + 'px';
        oBig.style.top = oEvent.clientY + 10 + 'px';

        //  設定對應的右邊放大圖片對應的位置
        oBigImg.style.left = l * (oBig.offsetWidth - oBigImg.offsetWidth)/(oSmall.offsetWidth-oMask.offsetWidth) + "px";
        oBigImg.style.top = t * (oBig.offsetHeight - oBigImg.offsetHeight)/(oSmall.offsetHeight-oMask.offsetHeight) + "px";
    }
}
</script>
%(heading)s
%(report)s
%(ending)s

</body>
</html>
"""
    # variables: (title, generator, stylesheet, heading, report, ending)

    # ------------------------------------------------------------------------
    # Stylesheet
    #
    # alternatively use a <link> for external style sheet, e.g.
    #   <link rel="stylesheet" href="$url" type="text/css">

    STYLESHEET_TMPL = """
<style type="text/css" media="screen">
body        { font-family: Microsoft YaHei,Tahoma,arial,helvetica,sans-serif;padding: 30px; font-size: 10px; }
table, th, td { border:1px solid #000 }
table       { font-size: 30px; }
table.result_table { border-collapse: collapse; }
table.result_table thead { background-color: #4f94d4; }
table.result_table th    { font-size: 14px; color: #FFF; padding: 0 5px; }
table.result_table td    { font-size: 12px; }
table td.title { font-weight: bold; font-size: 14px; }
.error-font { font-weight: bold; background-color: #f33; color: #fff; }

/* -- heading ---------------------------------------------------------------------- */
.heading {
    margin-top: 0ex;
    margin-bottom: 1ex;
}
.heading .description {
    margin-top: 4ex;
    margin-bottom: 6ex;
}
/* -- report ------------------------------------------------------------------------ */
#total_row  { font-weight: bold; }
.passCase   { color: #5cb85c; font-weight: bold; width: 20%; }
.failCase   { color: #d9534f; font-weight: bold; width: 20%; }
.errorCase  { color: #f0ad4e; font-weight: bold; width: 20%; }
.error, .error:hover      { background-color: #ffb300; color: #fff; border: none; font-weight: bold; }
.pass, .pass:hover       { background-color: #43a047; color: #fff; border: none; font-weight: bold; }
.fail, .fail:hover       { background-color: #f33; color: #fff; border: none; font-weight: bold; }
.none, .none:hover       { background-color: gray; color: #fff; border: none; font-weight: bold; }
.hiddenRow  { display: none; }
.testcase   { margin-left: 5px; font-size: 25px;}
.pass-text { font-weight:bold; color:green; }
.fail-text { font-weight:bold; color:red; }
.tag {
    font-weight: bold;
    padding: 2px 5px;
    font-size: 13px;
    letter-spacing: 1px;
    white-space: nowrap;
    color: #000;
    background: #ddd;
    border-radius: 3px;
}
.output-msg {
    background: #43a047;
    color: white;
}
.fail-msg {
    background: #f33;
    color: white;
}
.error-msg {
    background: #ffb300;
    color: white;
}
.img-container {
    display: inline-block;
    overflow-x: hidden;
    height:228px;
    margin-top: 45px;
}
.msg-container {
    flex: 1;
    display: inline-block;
    width: 90%;
    padding: 0 10px 0 20px;
}
.pre-text {
    white-space: pre-wrap;
    overflow-x: hidden;
    padding: 0 0 0 10px;
}

.screenshot {
    position: relative;
    margin-bottom: 10px;
}

.screenshot > div {
    position: absolute;
    top: 80%;
    left: 2%;
    font-weight: bold;
    color: #F00;
}
.screenshot > img {
    width: 400px;
    height: inherit;
    border: 1px solid #15c !important;
    cursor: pointer;
}
/* lightbox 燈箱 */
.modal-main {
    display: none;
    position: fixed;
    top: 0;
    left:0;
    width: 101%;
    height: 100%;
}

.modal-background {
    height: 100%;
    background-color: #000;
    opacity: 0.8;
}
.modal-body {
    position: absolute;
    top: 0;
    margin-top: 60px;
    width: 100%;
}
.modal-close {
    position: absolute;
    top: 20px;
    right: 100px;
    cursor: pointer;
    font-size: 35px;
    color: #FFF;
}
.light {
    position: relative;
    margin: 0 auto;
    width: 80%;
    max-width: 1200px;
}
/* 燈箱 banner box */
.foucsbox {
    overflow: hidden;
    white-space: nowrap;
    border-radius: 9px;
}
.foucsbox img {
    display: inline-block;
    margin-right: 24px;
    width: 100%;
    height: auto;
}
.prev,
.next {
    position: absolute;
    top: 50%;
    padding: 30px;
    cursor: pointer;
    font-size: 35px;
}
.prev {
    left: 10%;
}
.next {
    right: 10%;
}
.prev:hover,
.next:hover {
    cursor: pointer;
    border-radius: 40px;
    color: #FFF;
}
.indicat-menu {
    position: absolute;
    bottom: -25px;
    left: 50%;
    margin-left: -103px;
}
.column {
    display: inline-block;
    margin-right: 8px;
    cursor: pointer;
}
.indicat {
    width: 25px;
    height: 25px;
    border: 2px solid #FFF;
    border-radius: 15px;
    opacity: 0.5;
}
.indicat.current,
.indicat:hover {
    opacity: 0.9;
}
#mask{
    width: 75px;
    height: 75px;
    opacity: 0;
    background-color: #FF0;
    position: absolute;
    display: none;
}
#bigLightbox{
    width: 300px;
    height: 200px;
    border: 1px solid #FFF;
    overflow: hidden;
    float: left;
    position: fixed;
    display: none;
}
#bigLightbox img{
    position: absolute;
}
.zoom-in {
    cursor: -moz-zoom-in;
    cursor: -webkit-zoom-in;
    cursor: zoom-in;
}
/* Switch button */
.btn-default.btn-on.active {
    background-color: #5BB75B;
    color: white;
}
.btn-default.btn-off.active {
    background-color: #DA4F49;
    color: white;
}
</style>
"""

    # ------------------------------------------------------------------------
    # Heading
    #

    HEADING_TMPL = """<div class='heading'>
<h1 style="font-family: Microsoft YaHei">%(title)s</h1>
%(parameters)s
<p class='description'>%(description)s</p>
</div>

"""  # variables: (title, parameters, description)

    HEADING_ATTRIBUTE_TMPL = """<p class='attribute'><strong>%(name)s : </strong> %(value)s</p>
"""  # variables: (name, value)

    # ------------------------------------------------------------------------
    # Report
    #
    # 汉化,加美化效果 --Findyou
    REPORT_TMPL = """
<p id='show_detail_line'>
<a class="btn btn-primary" href='javascript:showCase(0)'>Passrate{ %(passrate)s }</a>
<a class="btn btn-danger" href='javascript:showCase(1)'>Failed{ %(fail)s }</a>
<a class="btn btn-success" href='javascript:showCase(2)'>Pass{ %(Pass)s }</a>
<a class="btn btn-warning" href='javascript:showCase(3)'>Error{ %(error)s }</a>
<a class="btn btn-info" href='javascript:showCase(4)'>All{ %(count)s }</a>
</p>
<table id='result_table' class="table table-condensed table-bordered table-hover">
<colgroup>
<col align='left' />
<col align='right' />
<col align='right' />
<col align='right' />
<col align='right' />
<col align='right' />
</colgroup>
<tr id='header_row' class="text-center success" style="font-weight: bold;font-size: 14px;">
    <td>Test Group/Test case</td>
    <td>Count</td>
    <td>Pass</td>
    <td>Fail</td>
    <td>Error</td>
    <td>View</td>
</tr>
%(test_list)s
<tr id='total_row' class="text-center active">
    <td>Total</td>
    <td>%(count)s</td>
    <td>%(Pass)s</td>
    <td>%(fail)s</td>
    <td>%(error)s</td>
    <td>Passrate：%(passrate)s</td>
</tr>
</table>
"""  # variables: (test_list, count, Pass, fail, error ,passrate)

    REPORT_CLASS_TMPL = r"""
<tr class='%(style)s warning'>
    <td>%(desc)s</td>
    <td class="text-center">%(count)s</td>
    <td class="text-center">%(Pass)s</td>
    <td class="text-center">%(fail)s</td>
    <td class="text-center">%(error)s</td>
    <td class="text-center"><a href="javascript:showClassDetail('%(cid)s',%(count)s)" class="detail" id='%(cid)s'>展開</a></td>
</tr>
"""  # variables: (style, desc, count, Pass, fail, error, cid)

    # 失败 的样式，去掉原来JS效果，美化展示效果  -Findyou
    REPORT_TEST_WITH_OUTPUT_TMPL = r"""
<tr id='%(tid)s' class='%(Class)s'>
    <td class='%(style)s'><div class='testcase'>%(desc)s</div></td>
    <td colspan='5' align='center'>
    <!--
    <button id='btn_%(tid)s' type="button"  class="btn btn-xs collapsed" data-toggle="collapse" data-target='#div_%(tid)s'>%(status)s</button>
    <div id='div_%(tid)s' class="collapse">  -->

    <!-- -->
    <button id='btn_%(tid)s' type="button"  class="btn btn-xs %(btn_style)s" data-toggle="collapse" data-target='#div_%(tid)s'>%(status)s</button>
    <div id='div_%(tid)s' class="collapse in">
    <div class="row">
    %(script)s
    </div>
    </div>
    </td>
</tr>
"""  # variables: (tid, Class, style, desc, status)

    # 通过 的样式，加标签效果  -Findyou
    REPORT_TEST_NO_OUTPUT_TMPL = r"""
<tr id='%(tid)s' class='%(Class)s'>
    <td class='%(style)s'><div class='testcase'>%(desc)s</div></td>
    <td colspan='5' align='center'><span class="label label-success success">%(status)s</span></td>
</tr>
"""  # variables: (tid, Class, style, desc, status)

    REPORT_TEST_OUTPUT_TMPL = r"""
%(output)s
"""  # variables: (id, output)

    # ------------------------------------------------------------------------
    # ENDING
    #
    # 增加返回顶部按钮  --Findyou
    ENDING_TMPL = """<div id='ending'>&nbsp;</div>
    <div style=" position:fixed;right:50px; bottom:30px; width:20px; height:20px;cursor:pointer">
    <a href="#"><span class="glyphicon glyphicon-eject" style = "font-size:30px;" aria-hidden="true">
    </span></a></div>
    <!-- 燈箱 modal -->
    <div id="modalbox" class="modal-main">
        <div class="modal-background"></div>
        <span class="modal-close" onclick="closeModal()">X</span>
        <div class="modal-body" >
            <div class="light">
                <!-- 燈箱圖片  -->
                <div id="lightbox" class="foucsbox">
                    <img src='' />
                    <span id="mask"></span>
                </div>
                <div id="bigLightbox">
                    <img src="" id="bigImg"/>
                </div>
            </div>
            <a class="prev" onclick="imageSwitch(-1, null, 0)">&#10094;</a>
            <a class="next" onclick="imageSwitch(1, null, 0)">&#10095;</a>
            <!-- 指標清單 -->
            <div id="indicators" class="indicat-menu"></div>
        </div>
    </div>
    """

# -------------------- The end of the Template class -------------------


class _TestResult(TestResult):
    # note: _TestResult is a pure representation of results.
    # It lacks the output and reporting ability compares to unittest._TextTestResult.

    def __init__(self, verbosity=1):
        TestResult.__init__(self)
        self.outputBuffer = io.StringIO()
        self.stdout0 = None
        self.stderr0 = None
        self.success_count = 0
        self.failure_count = 0
        self.error_count = 0
        self.verbosity = verbosity

        # result is a list of result in 4 tuple
        # (
        #   result code (0: success; 1: fail; 2: error),
        #   TestCase object,
        #   Test output (byte string),
        #   stack trace,
        # )
        self.result = []
        # 增加一个测试通过率 --Findyou
        self.passrate = float(0)

    def startTest(self, test):
        TestResult.startTest(self, test)
        # just one buffer for both stdout and stderr
        self.outputBuffer = io.StringIO()
        stdout_redirector.fp = self.outputBuffer
        stderr_redirector.fp = self.outputBuffer
        self.stdout0 = sys.stdout
        self.stderr0 = sys.stderr
        sys.stdout = stdout_redirector
        sys.stderr = stderr_redirector

    def complete_output(self):
        """
        Disconnect output redirection and return buffer.
        Safe to call multiple times.
        """
        if self.stdout0:
            sys.stdout = self.stdout0
            sys.stderr = self.stderr0
            self.stdout0 = None
            self.stderr0 = None
        return self.outputBuffer.getvalue()

    def stopTest(self, test):
        # Usually one of addSuccess, addError or addFailure would have been called.
        # But there are some path in unittest that would bypass this.
        # We must disconnect stdout in stopTest(), which is guaranteed to be called.
        self.complete_output()

    def addSuccess(self, test):
        self.success_count += 1
        TestResult.addSuccess(self, test)
        output = self.complete_output()
        self.result.append((0, test, output, ''))
        if self.verbosity > 1:
            if test.shortDescription():
                name = test.shortDescription()
            elif hasattr(test, '_testMethodName'):
                name = test._testMethodName
            else:
                name = str(test)
            sys.stderr.write(name)
            sys.stderr.write(' <span style="color: #43a047;">PASS</span>')
            sys.stderr.write('\n')
        else:
            sys.stderr.write('.')

    def addError(self, test, err):
        self.error_count += 1
        TestResult.addError(self, test, err)
        _, _exc_str = self.errors[-1]
        output = self.complete_output()
        self.result.append((2, test, output, _exc_str))
        if self.verbosity > 1:
            if test.shortDescription():
                name = test.shortDescription()
            elif hasattr(test, '_testMethodName'):
                name = test._testMethodName
            else:
                name = str(test)
            sys.stderr.write(name)
            sys.stderr.write(' <span style="color: #ffb300;">ERROR</span>')
            sys.stderr.write('\n')
        else:
            sys.stderr.write('E')

    def addFailure(self, test, err):
        self.failure_count += 1
        TestResult.addFailure(self, test, err)
        _, _exc_str = self.failures[-1]
        output = self.complete_output()
        self.result.append((1, test, output, _exc_str))
        if self.verbosity > 1:
            if test.shortDescription():
                name = test.shortDescription()
            elif hasattr(test, '_testMethodName'):
                name = test._testMethodName
            else:
                name = str(test)
            sys.stderr.write(name)
            sys.stderr.write(' <span style="color: #f33;">FAIL</span>')
            sys.stderr.write('\n')
        else:
            sys.stderr.write('F')


class HTMLTestRunner(Template_mixin):

    def __init__(self, stream=sys.stdout, verbosity=1, title=None, description=None, tester=None, driver=None):
        self.base64_dict = {}
        self.test_img = []
        self.img_list = []
        self.driver = driver
        self.stream = stream
        self.verbosity = verbosity
        if title is None:
            self.title = self.DEFAULT_TITLE
        else:
            self.title = title
        if description is None:
            self.description = self.DEFAULT_DESCRIPTION
        else:
            self.description = description
        if tester is None:
            self.tester = self.DEFAULT_TESTER
        else:
            self.tester = tester

        self.startTime = datetime.datetime.now()

        sys.settrace(self.screenshot)

    def get_img(self, msg=None):
        return msg

    def screenshot(self, frame, event, arg):
        """ 截圖 """

        if event == 'return':
            name = frame.f_code.co_name
            if self.driver and name.startswith('test_'):
                # value = self.driver.get_screenshot_as_base64()
                # buffer = io.BytesIO()
                # img = base64.b64decode(value)
                # img = Image.open(io.BytesIO(img))
                # img = img.resize((1400, 750),Image.ANTIALIAS)
                # img = img.convert('RGB')
                # img.save(buffer, format='JPEG')
                # img_b64 = base64.b64encode(buffer.getvalue())
                # value = str(img_b64)[2:-1]
                # img = f'screenshot-{int(datetime.datetime.now().timestamp())}'
                # self.base64_dict[img] = value
                # self.test_img.append(img)
                self.img_list.append([self.test_img, name])
                # print(f'測試結束 (圖{len(self.test_img) - 1})', end='')
                self.test_img = []
            elif self.driver and name == 'get_img':
                value = self.driver.get_screenshot_as_base64()
                buffer = io.BytesIO()
                img = base64.b64decode(value)
                img = Image.open(io.BytesIO(img))
                img = img.resize((1400, 750),Image.ANTIALIAS)
                img = img.convert('RGB')
                img.save(buffer, format='JPEG')
                img_b64 = base64.b64encode(buffer.getvalue())
                value = str(img_b64)[2:-1]
                img = f'screenshot-{int(datetime.datetime.now().timestamp())}'
                self.base64_dict[img] = value
                self.test_img.append(img)
                if arg:
                    print(f'{arg} (圖{len(self.test_img) - 1})\n')

        return self.screenshot

    def run(self, test):
        "Run the given test case or test suite."
        result = _TestResult(self.verbosity)
        test(result)
        # print(test)
        # print(result)
        self.stopTime = datetime.datetime.now()
        self.generateReport(test, result)
        print('\nTime Elapsed: %s' % (self.stopTime-self.startTime), file=sys.stderr)

        # Self added - Avoid same 'cid' in each currency run
        # (Which would case HTML hide function not work)
        global run_time_count
        run_time_count += 500

        return result

    def sortResult(self, result_list):
        # unittest does not seems to run in any particular order.
        # Here at least we want to group them together by class.
        rmap = {}
        classes = []
        for n, t, o, e in result_list:
            cls = t.__class__
            if cls not in rmap:
                rmap[cls] = []
                classes.append(cls)
            rmap[cls].append((n, t, o, e))
        r = [(cls, rmap[cls]) for cls in classes]
        return r

    # 替换测试结果status为通过率 --Findyou
    def getReportAttributes(self, result):
        """
        Return report attributes as a list of (name, value).
        Override this to add custom attributes.
        """
        startTime = str(self.startTime)[:19]
        duration = str(self.stopTime - self.startTime)
        status = []
        status.append('Total %s' % (result.success_count + result.failure_count + result.error_count))
        if result.success_count: status.append('Pass %s'    % result.success_count)
        if result.failure_count: status.append('Fail %s' % result.failure_count)
        if result.error_count:   status.append('Error %s'   % result.error_count)
        if status:
            status = '，'.join(status)
            self.passrate = str("%.2f%%" % (float(result.success_count) / float(result.success_count + result.failure_count + result.error_count) * 100))
        else:
            status = 'none'
        return [
            ('Tester', self.tester),
            ('Start Time', startTime),
            ('Duration', duration),
            ('Test Result', status + "，Pass rate= "+self.passrate),
        ]

    def generateReport(self, test, result):
        report_attrs = self.getReportAttributes(result)
        generator = 'HTMLTestRunner %s' % __version__
        stylesheet = self._generate_stylesheet()
        heading = self._generate_heading(report_attrs)
        report = self._generate_report(result)
        ending = self._generate_ending()
        output = self.HTML_TMPL % dict(
            title = saxutils.escape(self.title),
            generator = generator,
            stylesheet = stylesheet,
            heading = heading,
            report = report,
            ending = ending,
        )
        self.stream.write(output.encode('utf8'))

    def _generate_stylesheet(self):
        return self.STYLESHEET_TMPL

    # 增加Tester显示 -Findyou
    def _generate_heading(self, report_attrs):
        a_lines = []
        for name, value in report_attrs:
            line = self.HEADING_ATTRIBUTE_TMPL % dict(
                name = saxutils.escape(name),
                value = saxutils.escape(value),
            )
            a_lines.append(line)
        heading = self.HEADING_TMPL % dict(
            title = saxutils.escape(self.title),
            parameters = ''.join(a_lines),
            description = saxutils.escape(self.description),
            tester = saxutils.escape(self.tester),
        )
        return heading

    # 生成报告  --Findyou添加注释
    def _generate_report(self, result):

        # Self added - Avoid same 'cid' in each currency run
        # (Which would case HTML hide function not work)
        global run_time_count

        rows = []
        sortedResult = self.sortResult(result.result)
        for cid, (cls, cls_results) in enumerate(sortedResult):
            # subtotal for a class
            np = nf = ne = 0
            for n, t, o, e in cls_results:
                if n == 0: np += 1
                elif n == 1: nf += 1
                else: ne += 1

            # format class description
            if cls.__module__ == "__main__":
                name = cls.__name__
            else:
                name = "%s.%s" % (cls.__module__, cls.__name__)
            doc = cls.__doc__ and cls.__doc__.split("\n")[0] or ""
            desc = doc and doc or name

            row = self.REPORT_CLASS_TMPL % dict(
                style = ne > 0 and 'errorClass' or nf > 0 and 'failClass' or 'passClass',
                desc = desc,
                count = np+nf+ne,
                Pass = np,
                fail = nf,
                error = ne,
                # Self added - Avoid same 'cid' in each currency run
                # (Which would case HTML hide function not work)
                cid = 'c%s' % (cid+1 + run_time_count),
            )
            rows.append(row)
            for tid, (n, t, o, e) in enumerate(cls_results):
                self._generate_report_test(rows, cid, tid, n, t, o, e)

        report = self.REPORT_TMPL % dict(
            test_list = ''.join(rows),
            count = str(result.success_count+result.failure_count+result.error_count),
            Pass = str(result.success_count),
            fail = str(result.failure_count),
            error = str(result.error_count),
            passrate = self.passrate,
        )
        return report

    def _generate_report_test(self, rows, cid, tid, n, t, o, e):
        global hidde_status
        global image_url

        # Self added - Avoid same 'cid' in each currency run
        # (Which would case HTML hide function not work)
        global run_time_count

        # o print的内容
        # e 抛出的异常信息
        # t 具体的测试用例情况
        # e.g. 'pt1.1', 'ft1.1', etc
        has_output = bool(o or e)
        # print(has_output)
        # 当有print输入或者有异常抛出时都采用REPORT_TEST_WITH_OUTPUT_TMPL
        # ID修改点为下划线,支持Bootstrap折叠展开特效 - Findyou
        # print(n)

        # Self added - Avoid same 'cid' in each currency run
        # (Which would case HTML hide function not work)
        tid = (n == 0 and 'p' or n == 1 and 'f' or n == 2 and 'e') + 't%s_%s' % (cid+1 + run_time_count, tid+1)
        # print(tid)
        # tid = '%s_%s' % (cid+1,tid+1)
        picture_count = 0
        name = t.id().split('.')[-1]
        doc = t.shortDescription() or ""
        desc = doc and doc or name
        tmpl = has_output and self.REPORT_TEST_WITH_OUTPUT_TMPL or self.REPORT_TEST_NO_OUTPUT_TMPL

        if o:
            o = f"""<label class="tag output-msg">輸出訊息</label><br><pre class="pre-text">{o}</pre><br><br>"""
        else:
            o = ''

        if e:
            message = [item for item in e.split('\n') if item != '']
            if 'AssertionError' in e:
                message = re.findall(r'(AssertionError:(.|\n)*?$)', e)
                message = message[0][0] if len(message) > 0 and len(message[0]) > 0 else e
                e = f"""<label class="tag fail-msg">FAIL原因</label><br><pre class="pre-text">{message}</pre><br><br>"""
            else:
                file_info = '\n'.join([', '.join(item) for item in re.findall(r'File\s"(.*)",\s(line\s\d+),\s(.+)', e) if 'vir' in item[0]])
                exc = re.findall(r'\.(\w*\:.*)\n*$', e)
                message = file_info + '\n' + exc[0] if len(exc) > 0 else e
                e = f"""<label class="tag error-msg">ERROR原因</label><br><pre class="pre-text">{message}</pre><br><br>"""
        else:
            e = ''

        s = ''
        if self.driver:
            if self.img_list and self.img_list[0][1] == name:
                for index, name in enumerate(self.img_list.pop(0)[0]):
                    picture_count += 1
                    s += f"""<div class="screenshot" onclick='goLightBox(this)'><img src="data:image/png;base64, {self.base64_dict[name]}"/><div>{index}</div></div>"""

                s = '<div class="col-md-5"><div class="img-container">' + s + '</div></div>'
            o = '<div class="col-md-7"><div class="msg-container">' + (o + e) + '</div></div>'
        else:
            o = '<div class="col-md-12"><div class="msg-container">' + (o + e) + '</div></div>'

        script = self.REPORT_TEST_OUTPUT_TMPL % dict(
            # output = saxutils.escape(uo+ue),
            output = (s + o)
        )

        row = tmpl % dict(
            tid = tid,
            Class = (n == 0 and 'hiddenRow' or ''),
            style = n == 2 and 'errorCase' or (n == 1 and 'failCase' or 'passCase'),
            btn_style = n == 2 and 'error' or (n == 1 and 'fail' or 'pass'),
            desc = desc,
            script = script,
            status = self.STATUS[n].upper())
        rows.append(row)
        if not has_output:
            return

    def _generate_ending(self):
        return self.ENDING_TMPL


##############################################################################
# Facilities for running tests from the command line
##############################################################################

# Note: Reuse unittest.TestProgram to launch test. In the future we may
# build our own launcher to support more specific command line
# parameters like test title, CSS, etc.
class TestProgram(unittest.TestProgram):
    """
    A variation of the unittest.TestProgram. Please refer to the base
    class for command line parameters.
    """

    def runTests(self):
        # Pick HTMLTestRunner as the default test runner.
        # base class's testRunner parameter is not useful because it means
        # we have to instantiate HTMLTestRunner before we know self.verbosity.
        if self.testRunner is None:
            self.testRunner = HTMLTestRunner(verbosity=self.verbosity)
        unittest.TestProgram.runTests(self)


main = TestProgram

##############################################################################
# Executing this module from the command line
##############################################################################

if __name__ == "__main__":
    main(module=None)
