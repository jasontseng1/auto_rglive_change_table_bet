# _*_ coding: UTF-8 _*_
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains
import time
import decimal


class LibraryFunctions:
    """ 共用函式 """

    def __init__(self, driver, get_img):
        self.driver = driver
        self.get_img = get_img

    @staticmethod
    def check_all_round(all_round):
        """
        確認所有下注注區是否為 True
        參數:
            all_round (dict): 遊戲注區

        Returns:
            bool: 判斷全部成功回傳 True，失敗則是 False
        """

        result_list = [all_round[i]['bet_finish'] for i in all_round]
        return False if "False" in str(result_list) else True

    def bet_and_check_balance(self, table_data, bet_xpath, wait_time=30):
        """
        下注及確認餘額
        參數:
            table_data (dict): 桌別資料
            bet_xpath (str): 下注xpath
            wait_tome (int): 等待秒數，預設為30秒

        Returns:
            dict: 桌別資料
        """

        count = 0
        # 使用迴圈來下注(迴圈最多跑兩次)
        while count < 2:
            try:
                # 確認是否已完成
                if table_data['bet_finish']:
                    continue

                # 抓取當前局號
                now_round = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='game-name']"))).text
                bet_round = now_round[4:]

                # 撈取倒數秒數
                bet_sec = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='timer-wrapper']"))).text

                # 撈取餘額
                old_money = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//div[@class='input balance']/span"))).text
                old_money = decimal.Decimal(old_money.replace(',', ''))

                # 當倒數秒數不為空且大於3秒時才下注
                if not bet_sec == "" and int(bet_sec) > 3:

                    # 下注
                    WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, f"//*[name()='svg' and contains(@class, 'touch-panel')]{bet_xpath}"))).click()
                    time.sleep(1)

                    # 點擊確認
                    WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                        (By.XPATH, "//img[@src='img/confirm_button.5fe259e2.svg']"))).click()

                    # 判斷下注成功提示是否有出現
                    msg = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[contains(text(), '下注成功')]"))).text

                    bet_money = decimal.Decimal(msg[5:])

                    new_money = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@class='input balance']/span"))).text
                    new_money = decimal.Decimal(new_money.replace(',', ''))

                    # 判斷餘額是否正確
                    if new_money + bet_money == old_money:
                        self.get_img("下注餘額正確")
                        table_data['bet_finish'] = True
                        table_data['round'] = bet_round
                        table_data['balance'] = str(bet_money)
                    else:
                        self.get_img("Fail: 餘額有誤")
                        print(f"下注前餘額:{old_money}")
                        print(f"下注金額:{bet_money}")
                        print(f"下注後餘額:{new_money}")

                    # 等待結算 or 當局取消 or 中場休息
                    ele = WebDriverWait(self.driver, wait_time).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@class='ingame-dialogue finish' or "
                                   "@class='ingame-dialogue cancel' or @class='ingame-dialogue intermission']")))
                    if 'finish' in ele.get_attribute('class'):
                        time.sleep(5)
                    else:
                        time.sleep(2)

                    # 結束迴圈
                    count += 2
                else:
                    # 等待結算 or 當局取消 or 中場休息
                    WebDriverWait(self.driver, wait_time).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@class='ingame-dialogue finish' or "
                                   "@class='ingame-dialogue cancel' or @class='ingame-dialogue intermission']")))
                    time.sleep(1)

                    # 等待新局出現
                    WebDriverWait(self.driver, wait_time).until(EC.presence_of_element_located(
                        (By.XPATH, "//div[@class='ingame-dialogue countdown']")))
                    time.sleep(2)

                # 計算下注次數
                count += 1
            except:
                count += 2
                self.get_img("Fail: 下注失敗")

        return table_data

    def check_report(self, all_bet_data):
        """
        確認報表是否有下注資料
        參數:
            all_bet_data (dict): 下注資料

        Returns:
            bool: 判斷全部成功回傳 True，失敗則是 False
        """

        # init
        ac = ActionChains(self.driver)
        field_dict = {}

        # 先等5秒等最後一筆資料出現
        time.sleep(5)

        try:
            # 點開報表
            report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@aria-label='報表']")))
            ac.move_to_element(report).click().perform()
            time.sleep(1)

            try:
                # 資料欄位
                th_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(
                    (By.XPATH, "//table[@id='vgt-table']//thead//th")))
            except:
                # 關閉報表
                close_report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@class='box bet-record']//img[@class='close']")))
                ac.move_to_element(close_report).click().perform()
                time.sleep(1)

                # 點開報表
                report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                    (By.XPATH, "//div[@aria-label='報表']")))
                ac.move_to_element(report).click().perform()
                time.sleep(1)

                # 資料欄位
                th_list = WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located(
                    (By.XPATH, "//table[@id='vgt-table']//thead//th")))

            for index, ele in enumerate(th_list):
                # 取特定欄位
                if ele.text in ['局號', '桌號', '下注金額']:
                    # 報表欄位顯示差異，故不用+1
                    field_dict[ele.text] = index

            # 反轉下注資料排序
            all_bet_data = dict(sorted(all_bet_data.items(), key=lambda x: x[0], reverse=True))

            # 取得頁數資訊
            ele = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='footer__navigation__page-info']")))
            now_page, total_page = ele.text.replace(' ', '').split('-')[-1].split('/')
            # 當前頁數
            now_page = int(now_page)
            # 總頁數
            total_page = int(total_page)

            print(f"==================== 檢查項目：「下注資料」與「報表」比對 ==================== \n")
            page_count = 0
            while page_count < total_page:
                # 截圖判斷用
                check_list = [all_bet_data[_]['report_check'] for _ in all_bet_data]

                for i in all_bet_data:
                    if all_bet_data[i]['bet_finish'] and not all_bet_data[i]['report_check']:
                        print(f"======= 桌別:{i} =======")

                        try:
                            tr = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                                (By.XPATH, f"//td[{field_dict['桌號']}]"
                                           f"/span[contains(normalize-space(text()), '{i}')]/../..")))

                            report_round = tr.find_element(By.XPATH, f"./td[{field_dict['局號']}]").text
                            report_bet = tr.find_element(By.XPATH, f"./td[{field_dict['下注金額']}]").text

                            # 比對局號
                            if report_round != all_bet_data[i]['round'] and now_page == total_page:
                                print(f"局號不同")
                                print(f"報表-局號:{report_round}")
                                print(f"投注-局號:{all_bet_data[i]['round']}")
                            # 比對金額
                            elif float(all_bet_data[i]['balance']) != float(report_bet) and now_page == total_page:
                                print(f"下注金額不同")
                                print(f"報表-下注金額:{float(report_bet)}")
                                print(f"投注-下注金額:{float(all_bet_data[i]['balance'])}")
                            else:
                                print("*** 檢查結束 ***\n")
                                all_bet_data[i]['report_check'] = True
                        except:
                            if now_page == total_page:
                                print(f"Fail: 「{i}」核對報表資料失敗")
                                print(f"投注-局號:{all_bet_data[i]['round']}\n")
                            else:
                                print(f"本頁無注單資訊\n")

                # 換頁前進行截圖
                if check_list != [all_bet_data[i]['report_check'] for i in all_bet_data]:
                    self.get_img("報表資訊")

                # 全部都True 結束判斷
                if "False" not in str([all_bet_data[_]['report_check'] for _ in all_bet_data]):
                    break
                elif now_page != total_page:
                    count = 0
                    while count < 2:
                        # 換頁
                        _page = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                            (By.XPATH, "//span[text()='下一頁']")))
                        ac.move_to_element(_page).click().perform()
                        time.sleep(1)
                        _ele = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(
                            (By.XPATH, "//div[@class='footer__navigation__page-info']")))
                        time.sleep(1)

                        # 新頁碼, 新總頁數
                        new_page, new_total_page = _ele.text.replace(' ', '').split('-')[-1].split('/')
                        new_page = int(new_page)
                        new_total_page = int(new_total_page)

                        # 判斷總頁數是否有變化
                        if total_page != new_total_page:
                            total_page = new_total_page
                            # 重新點擊搜尋
                            search = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                                (By.XPATH, "//img[@src='img/search-solid.9c4d71fa.svg']")))
                            ac.move_to_element(search).click().perform()
                            time.sleep(1)
                            # 重新設定
                            page_count = 0
                            count += 2

                        elif now_page == new_page:
                            count += 1
                            if count == 2:
                                self.get_img("Fail：注單換頁失敗")
                                page_count = total_page + 1
                        else:
                            now_page = new_page
                            count += 2
                            page_count += 10
                else:
                    page_count = total_page + 1

        except:
            self.get_img("Fail: 報表比對失敗")

        try:
            WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(
                (By.XPATH, "//div[@class='box bet-record']")))

            # 關閉報表
            close_report = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//div[@class='box bet-record']//img[@class='close']")))
            ac.move_to_element(close_report).click().perform()
            time.sleep(1)
        except:
            self.driver.refresh()

        result_list = [all_bet_data[i]['report_check'] for i in all_bet_data]
        return False if "False" in str(result_list) else True
