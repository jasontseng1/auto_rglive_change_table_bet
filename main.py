# _*_ coding: UTF-8 _*_
import sys
import os
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from Setting import *
from HTMLTestRunner import HTMLTestRunner
from datetime import datetime
from Games import Baccarat, DragonTiger, Roulette, SicBo, BlockChain, FeaturedGames

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "..")))
options = webdriver.ChromeOptions()
options.add_experimental_option('excludeSwitches', ['enable-automation'])
driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
get_img = HTMLTestRunner().get_img
driver.maximize_window()
driver.get(test_data['url'])


class LiveChangeTableBet(unittest.TestCase):
    """ RG真人換桌下注 """

    l_baccarat_ct_b = Baccarat.LiveBaccaratChangeTableBet(driver, get_img)
    l_dragontiger_ct_b = DragonTiger.LiveDragonTigerChangeTableBet(driver, get_img)
    l_roulette_ct_b = Roulette.LiveRouletteChangeTableBet(driver, get_img)
    l_sicbo_ct_b = SicBo.LiveSicBoChangeTableBet(driver, get_img)
    l_featuredgames_ct_b = FeaturedGames.LiveFeaturedGamesChangeTableBet(driver, get_img)
    l_blockchain_ct_b = BlockChain.LiveBlockChainChangeTableBet(driver, get_img)

    @classmethod
    def setUpClass(cls) -> None:
        # 測試是否繼續變數
        cls.test_continue = True

    def setUp(self) -> None:
        """ 每個測試項目測試之前調用 """

        # 判斷測試是否繼續
        if not self.__class__.test_continue:
            self.skipTest("中斷測試")

    def test_login(self):
        """ 登入 """

        result = True

        try:
            get_img("首頁")
            # 點擊語系
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[@class='txt_lang']"))).click()
            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//span[text()='繁體中文']"))).click()
            input_acc = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入帳號']")))
            input_acc.send_keys(test_data['account'][0])

            input_pwd = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//input[@class='ipt_login' and @placeholder='請輸入密碼']")))
            input_pwd.send_keys(test_data['account'][1])

            WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
                (By.XPATH, "//button[@type='submit']"))).click()
            time.sleep(1)

            # 判斷輸入框隱藏
            WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//form[@class='are_head' and @style='display: none;']")))

            check_name = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
                (By.XPATH, "//div[@class='ui_username']")))

            if test_data['account'][0] not in check_name.text:
                print(f"Fail: 登入失敗，登入帳號:{test_data['account'][0]}，顯示帳號為:{check_name.text}")
                result = False
                self.__class__.test_continue = False

            get_img("登入成功")

        except:
            get_img("登入失敗")
            result = False
            self.__class__.test_continue = False

        self.assertEqual(True, result)

    def test_login_RGLive(self):
        """ 進入RG真人、檢查餘額 """

        result = True

        # 點擊餘額
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//i[@class='i_refresh']"))).click()
        time.sleep(5)
        WebDriverWait(driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//span[@class='txt_yellow']")))
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='重新檢查餘額']"))).click()

        # wait loading
        WebDriverWait(driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
        time.sleep(1)

        # 撈取官網餘額
        H1_money = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
            (By.XPATH, "//div[@class='txt_balance']"))).text
        print(f"官網餘額：{H1_money}")

        # 關閉視窗
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//a[@class='box_close']"))).click()

        # 點擊真人類別->進入RG真人
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//span[text()='真人']"))).click()
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
            (By.XPATH, "//div[@class='reg_live rg']//a[@class='lnk_enterGame']"))).click()

        # wait loading
        WebDriverWait(driver, 10).until(EC.invisibility_of_element_located(
            (By.XPATH, "//div[@class='box_bg box_loading' and not(contains(@style, 'none'))]")))
        time.sleep(1)

        # 判斷是否有餘額刷新彈窗出現，若出現代表無法進入遊戲
        try:
            WebDriverWait(driver, 5).until(EC.presence_of_element_located(
                (By.XPATH, "//p[text()='餘額刷新中，請稍候']")))
            get_img("Fail：進RG真人失敗，出現「餘額刷新中，請稍後」彈出視窗")
            result = False
            self.__class__.test_continue = False

        except:
            pass

        self.assertEqual(True, result)

        # 切換分頁
        handles = driver.window_handles
        driver.switch_to.window(handles[-1])

        # 撈取RG餘額
        RG_money = WebDriverWait(driver, 10).until(EC.presence_of_element_located(
            (By.XPATH, "//div[@class='input balance']/span"))).text
        RG_money = RG_money[:-1]
        print(f"RG真人餘額：{RG_money}")

        # 餘額比對
        print("測試項目：餘額是否帶入遊戲")
        if H1_money == RG_money:
            get_img("餘額成功帶入遊戲")
        else:
            get_img("Fail：遊戲餘額與官網餘額不符，測試失敗")
            result = False
            self.__class__.test_continue = False

        self.assertEqual(True, result)

    def test_baccarat_lobby(self):
        """ 百家樂大廳換桌下注 """

        result = self.l_baccarat_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_baccarat_inside(self):
        """ 百家樂遊戲內換桌下注 """

        result = self.l_baccarat_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)

    def test_dragontiger_lobby(self):
        """ 龍虎大廳換桌下注 """

        result = self.l_dragontiger_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_dragontiger_inside(self):
        """ 龍虎遊戲內換桌下注 """

        result = self.l_dragontiger_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)

    def test_roulette_lobby(self):
        """ 輪盤大廳換桌下注 """

        result = self.l_roulette_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_roulette_inside(self):
        """ 輪盤遊戲內換桌下注 """

        result = self.l_roulette_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)

    def test_sicbo_lobby(self):
        """ 骰寶大廳換桌下注 """

        result = self.l_sicbo_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_sicbo_inside(self):
        """ 骰寶遊戲內換桌下注 """

        result = self.l_sicbo_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)

    def test_featuredgames_lobby(self):
        """ 特色遊戲大廳換桌下注 """

        result = self.l_featuredgames_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_featuredgames_inside(self):
        """ 特色遊戲遊戲內換桌下注 """

        result = self.l_featuredgames_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)

    def test_blockchain_lobby(self):
        """ 區塊鏈大廳換桌下注 """

        result = self.l_blockchain_ct_b.lobby_change_table_bet()
        self.assertEqual(True, result)

    def test_blockchain_inside(self):
        """ 區塊鏈遊戲內換桌下注 """

        result = self.l_blockchain_ct_b.game_inside_change_table_bet()
        self.assertEqual(True, result)


if __name__ == '__main__':
    test_units = unittest.TestSuite()
    test_units.addTests([
        LiveChangeTableBet("test_login"),
        LiveChangeTableBet("test_login_RGLive"),
        LiveChangeTableBet("test_baccarat_lobby"),
        LiveChangeTableBet("test_baccarat_inside"),
        LiveChangeTableBet("test_dragontiger_lobby"),
        LiveChangeTableBet("test_dragontiger_inside"),
        LiveChangeTableBet("test_roulette_lobby"),
        LiveChangeTableBet("test_roulette_inside"),
        LiveChangeTableBet("test_sicbo_lobby"),
        LiveChangeTableBet("test_sicbo_inside"),
        LiveChangeTableBet("test_featuredgames_lobby"),
        LiveChangeTableBet("test_featuredgames_inside"),
        LiveChangeTableBet("test_blockchain_lobby"),
        LiveChangeTableBet("test_blockchain_inside"),
    ])

    now = datetime.now().strftime('%m-%d %H_%M_%S')
    filename = './Report/' + now + '.html'
    with open(filename, 'wb+') as fp:
        runner = HTMLTestRunner(
            stream=fp,
            verbosity=2,
            title='換桌下注',
            driver=driver
        )
        runner.run(test_units)

    driver.quit()
